# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-bigscreen package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-bigscreen\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-11 00:43+0000\n"
"PO-Revision-Date: 2023-06-15 10:32+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Freek de Kruijf - 2021;2023"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "freekdekruijf@kde.nl"

#: ui/delegates/DatePicker.qml:110
#, kde-format
msgid "Jan"
msgstr "jan"

#: ui/delegates/DatePicker.qml:110
#, kde-format
msgid "Feb"
msgstr "feb"

#: ui/delegates/DatePicker.qml:110
#, kde-format
msgid "Mar"
msgstr "mrt"

#: ui/delegates/DatePicker.qml:110
#, kde-format
msgid "Apr"
msgstr "apr"

#: ui/delegates/DatePicker.qml:110
#, kde-format
msgid "May"
msgstr "mei"

#: ui/delegates/DatePicker.qml:110
#, kde-format
msgid "Jun"
msgstr "jun"

#: ui/delegates/DatePicker.qml:110
#, kde-format
msgid "Jul"
msgstr "jul"

#: ui/delegates/DatePicker.qml:110
#, kde-format
msgid "Aug"
msgstr "aug"

#: ui/delegates/DatePicker.qml:110
#, kde-format
msgid "Sep"
msgstr "sep"

#: ui/delegates/DatePicker.qml:110
#, kde-format
msgid "Oct"
msgstr "okt"

#: ui/delegates/DatePicker.qml:110
#, kde-format
msgid "Nov"
msgstr "nov"

#: ui/delegates/DatePicker.qml:110
#, kde-format
msgid "Dec"
msgstr "dec"

#: ui/DeviceTimeSettings.qml:149
#, kde-format
msgid "Time Display"
msgstr "Tijdweergave"

#: ui/DeviceTimeSettings.qml:156
#, kde-format
msgid "Timezone:"
msgstr "Tijdzone:"

#: ui/DeviceTimeSettings.qml:176
#, kde-format
msgid "Set time automatically:"
msgstr "Tijd automatisch instellen:"

#: ui/DeviceTimeSettings.qml:210
#, kde-format
msgid "Time"
msgstr "Tijd"

#: ui/DeviceTimeSettings.qml:242
#, kde-format
msgid "Date"
msgstr "Datum"

#: ui/DeviceTimeSettings.qml:315
#, kde-format
msgid "Press the [←] Back button to return to appearance settings"
msgstr ""
"Druk op de knop [←] Terug om naar instellingen voor uiterlijk terug te keren"

#: ui/DeviceTimeSettings.qml:378 ui/DeviceTimeSettings.qml:503
#: ui/DeviceTimeSettings.qml:600
#, kde-format
msgid "Press the [←] Back button to save configuration and return to settings"
msgstr ""
"Druk op de knop [←] Terug om configuratie op te slaan en naar instellingen "
"terug te keren"

#: ui/DeviceTimeSettings.qml:419
#, kde-format
msgid "Your local timezone is %1"
msgstr "Uw lokale tijdzone is %1"

#: ui/DeviceTimeSettings.qml:419
#, kde-format
msgid "%1, %2"
msgstr "%1, %2"

#: ui/main.qml:21
#, kde-format
msgid "Appearance"
msgstr "Uiterlijk"

#: ui/main.qml:105
#, kde-format
msgid "Exit"
msgstr "Afsluiten"

#: ui/main.qml:146
#, kde-format
msgid "Launcher Appearance"
msgstr "Uiterlijk van Programmastarter"

#: ui/main.qml:192
#, kde-format
msgid "Power Inhibition"
msgstr "Energie remmen"

#: ui/main.qml:211
#, kde-format
msgid "Colored Tiles"
msgstr "Gekleurde tegels"

#: ui/main.qml:230
#, kde-format
msgid "Expanding Tiles"
msgstr "Zich uitbreidende tegels"

#: ui/main.qml:248
#, kde-format
msgid "Adjust Date & Time"
msgstr "Datum & tijd aanpassen"

#: ui/main.qml:275
#, kde-format
msgid "General Appearance"
msgstr "Algemeen uiterlijk"
