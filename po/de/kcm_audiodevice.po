# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-bigscreen package.
# Frederik Schwarzer <schwarzer@kde.org>, 2020, 2022.
# Burkhard Lück <lueck@hube-lueck.de>, 2020, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-bigscreen\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-14 00:43+0000\n"
"PO-Revision-Date: 2022-04-15 04:47+0200\n"
"Last-Translator: Frederik Schwarzer <schwarzer@kde.org>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#: ui/delegates/CompactAudioDelegate.qml:87
#, kde-format
msgctxt "label of device items"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: ui/delegates/VolumeObject.qml:58
#, kde-format
msgid "Adjust Volume"
msgstr "Lautstärke anpassen"

#: ui/delegates/VolumeObject.qml:123
#, kde-format
msgctxt "volume percentage"
msgid "%1%"
msgstr "%1%"

#: ui/DeviceChooserPage.qml:74
#, kde-format
msgid "Exit"
msgstr "Beenden"

#: ui/DeviceChooserPage.qml:127
#, kde-format
msgid "Playback Devices"
msgstr "Wiedergabegeräte"

#: ui/DeviceChooserPage.qml:159
#, kde-format
msgid "Recording Devices"
msgstr "Aufnahmegeräte"

#: ui/main.qml:20
#, kde-format
msgid "Audio Device Chooser"
msgstr "Audiogerät auswählen"

#: ui/SettingsItem.qml:234
#, kde-format
msgid "Press the [←] Back button to return to device selection"
msgstr "Drücken Sie den Knopf  [←]  „Zurück“ bringt Sie zur Geräteauswahl"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Deutsches KDE-Übersetzerteam"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "kde-i18n-de@kde.org"

#~ msgid "Configure Plasma Audio"
#~ msgstr "Plasma-Audio einrichten"

#~ msgid "Connected"
#~ msgstr "Verbunden"

#~ msgid "Network"
#~ msgstr "Netzwerk"

#~ msgid "Refresh"
#~ msgstr "Aktualisieren"

#~ msgid "Enter Password For %1"
#~ msgstr "Passwort für %1 eingeben"

#~ msgid "Password..."
#~ msgstr "Passwort ..."

#~ msgid "Connect"
#~ msgstr "Verbinden"

#~ msgid "Cancel"
#~ msgstr "Abbrechen"

#~ msgid "Are you sure you want to forget the network %1?"
#~ msgstr "Möchten Sie das Netzwerk %1 wirklich verwerfen?"

#~ msgid "Forget"
#~ msgstr "Verwerfen"

#~ msgid "Connections"
#~ msgstr "Verbindungen"
